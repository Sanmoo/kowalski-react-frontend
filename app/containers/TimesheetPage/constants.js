/*
 *
 * TimesheetPage constants
 *
 */

export const DEFAULT_ACTION = 'app/TimesheetPage/DEFAULT_ACTION';
export const NEW_DATE_SELECTED = 'app/TimesheetPage/NEW_DATE_SELECTED';
export const NEXT_MONTH_CLICKED = 'app/TimesheetPage/NEXT_MONTH_CLICKED';
export const PREVIOUS_MONTH_CLICKED = 'app/TimesheetPage/PREVIOUS_MONTH_CLICKED';
export const SUBMIT_LOG_FORM = 'app/TimesheetPage/SUBMIT_LOG_FORM';
export const NEW_LOG_SAVED = 'app/TimesheetPage/NEW_LOG_SAVED';
export const DATE_DAY_FORMAT = 'D-M-YYYY';
