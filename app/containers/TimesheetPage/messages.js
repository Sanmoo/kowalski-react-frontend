/*
 * TimesheetPage Messages
 *
 * This contains all the text for the TimesheetPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  timesheetLabel: {
    id: 'app.containers.TimesheetPage.timesheetLabel',
  },
  notifications: {
    id: 'app.containers.TimesheetPage.notifications',
  },
});
