/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  timesheet: {
    id: 'app.components.navbar.timesheet',
  },
  projects: {
    id: 'app.components.navbar.projects',
  },
  people: {
    id: 'app.components.navbar.people',
  },
  usernamePlaceholder: {
    id: 'app.components.navbar.usernamePlaceholder',
  },
  appTitle: {
    id: 'app.title',
  },
});
