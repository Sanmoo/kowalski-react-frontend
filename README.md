# Quick Start
```shell
yarn install
yarn run start
```

# Implemented Screens
  * /auth (Sign in)
  * / (Timesheet page)
  * /log (Log hour form)

# Contribution Guidelines
  * Fork the project.
  * Branch master and start working on your feature.
  * Please, follow these [guidelines](https://chris.beams.io/posts/git-commit/) when writing commit messages.
  * Make sure your branch do not contain merge commits.
  * Open your PR!
